<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    use HasFactory;

    protected $table = 'program';
    public $timestamps = false;

    protected $fillable = [
        'program',
        'semesters',
        'campus',
        'faculty',
        'imgSrc',
    ];
}
