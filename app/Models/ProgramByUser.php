<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProgramByUser extends Model
{
    use HasFactory;

    protected $table = 'programByUser';
    public $timestamps = false;

    protected $fillable = [
        'id_user',
        'id_program',
    ];

    static function getStundetnsByProgram()
    {
        $programs = DB::select('select p.program, pu.id_program, count(pu.id_user) as count 
        from programByUser pu
        inner join program p on pu.id_program = p.id
        group by pu.id_program, p.program');

        return $programs;
    }

    static function getPRogramsByUser($id_user)
    {
        $programs = DB::select('select p.program
        from programByUser pu
        inner join program p on pu.id_program = p.id
        where pu.id_user ='.$id_user);

        $programsName = [];

        foreach ($programs as $p)
        {
            array_push($programsName, $p->program);
        }

        return $programsName;
    }
}
