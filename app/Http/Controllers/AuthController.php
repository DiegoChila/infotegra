<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use GuzzleHttp\Client;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $email = $request->email ? $request->email : null;
        $password = $request->password ? $request->password : null;
        if ($email && $password)
        {
            $jwt = JWTAuth::attempt(['email' => $email, 'password' => $password]);
            if ($jwt)
            {
                $user = User::where('email','=',$email)->first();
                return response()->json([
                    'login' => true,
                    'user' => $user,
                    'token' => $jwt,
                    'error' => ''
                ]);
            }
            else
            {
                return response()->json([
                    'login' => false,
                    'user' => '',
                    'token' => '',
                    'error' => 'credencials'
                ]);
            }
        }
        else
        {
            return response()->json([
                'login' => false,
                'user' => '',
                'token' => '',
                'error' => 'data'
            ]);
        }
    }

    public function register(Request $request)
    {
        $name = $request->name ? $request->name : null;
        $lastName = $request->lastName ? $request->lastName : null;
        // $code = $request->code ? $request->code : null;
        $direction = $request->direction ? $request->direction : null;
        $tel = $request->tel ? $request->tel : null;
        $cityResidence = $request->cityResidence ? $request->cityResidence : null;
        $cityOrigin = $request->cityOrigin ? $request->cityOrigin : null;
        $nationality = $request->nationality ? $request->nationality : null;
        $role = $request->role ? $request->role : null;
        $email = $request->email ? $request->email : null;
        $password = $request->password ? bcrypt($request->password) : null;

        if ($name && $lastName && $direction && $tel && $cityResidence && $cityOrigin && $nationality && $role && $email && $password)
        {
            $existsEmail = User::where('email','=',$email)->get();

            if ($existsEmail->isEmpty())
            {
                $existsCode = true;
                $code = 0;
                while($existsCode)
                {
                    $code = rand(10000, 99999);
                    $codeInDB = User::where('code','=',$code)->get();
                    if ($codeInDB->isEmpty())
                    {
                        $existsCode = false;
                    }
                }
                $user = new User();
                $user->name = $name;
                $user->lastName = $lastName;
                $user->code = $code;
                $user->direction = $direction;
                $user->tel = $tel;
                $user->cityResidence = $cityResidence;
                $user->cityOrigin = $cityOrigin;
                $user->nationality = $nationality;
                $user->role = $role;
                $user->email = $email;
                $user->password = $password;
                $user->save();

                $jwt = JWTAuth::attempt(['email' => $email, 'password' => $request->password]);
                if ($jwt)
                {
                    return response()->json([
                        'success' => true,
                        'user' => $user,
                        'token' => $jwt,
                        'error' => ''
                    ]);
                }
                else
                {
                    return response()->json([
                        'success' => false,
                        'user' => '',
                        'token' => '',
                        'error' => 'credencials'
                    ]);
                }
            }
            else
            {
                return response()->json([
                    'success' => false,
                    'user' => '',
                    'token' => '',
                    'error' => 'email'
                ]);
            }  
        }
        else
            {
                return response()->json([
                    'success' => false,
                    'user' => '',
                    'token' => '',
                    'error' => 'data'
                ]);
            }  
    }

    public function validateCaptcha(Request $request)
    {
        $secret = '6LcxY1QaAAAAAMBaQHO_OC4DklYiFOS9BWQ2kxum';
        $client = new Client();
        $validate = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
            'form_params' => [
                'secret' => $secret,
                'response' => $request->response,
            ]
        ]);

        $resp = json_decode($validate->getBody()->getContents());
        return response()->json([
            'success' => $resp->success,
            'value' => $request->response
        ]);
    }
}
