<?php

namespace App\Http\Controllers;

use App\Models\ProgramByUser;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stundet = User::where('role','=',2)->get();
        return response()->json([
            'stundent' => $stundet
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if ($user != null)
        {
            $programs = ProgramByUser::getPRogramsByUser($id);

            return response()->json([
                'success' => true,
                'user' => $user,
                'programs' => $programs,
                'error' => ''
            ]);
        }
        else
        {
            return response()->json([
                'success' => false,
                'user' => '',
                'programs' => '',
                'error' => 'user'
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->name ? $request->name : null;
        $lastName = $request->lastName ? $request->lastName : null;
        $direction = $request->direction ? $request->direction : null;
        $tel = $request->tel ? $request->tel : null;
        $cityResidence = $request->cityResidence ? $request->cityResidence : null;
        $cityOrigin = $request->cityOrigin ? $request->cityOrigin : null;
        $nationality = $request->nationality ? $request->nationality : null;

        if ($name && $lastName && $direction && $tel && $cityResidence && $cityOrigin && $nationality)
        {
            $user = User::find($id);

            if ($user!=null)
            {
                $user->name = $name;
                $user->lastName = $lastName;
                $user->direction = $direction;
                $user->tel = $tel;
                $user->cityResidence = $cityResidence;
                $user->cityOrigin = $cityOrigin;
                $user->nationality = $nationality;
                $user->save();

                $confirmUpdate = User::where('id','=',$id)
                ->where('name','=',$name)
                ->where('lastName','=',$lastName)
                ->where('direction','=',$direction)
                ->where('tel','=',$tel)
                ->where('cityResidence','=',$cityResidence)
                ->where('cityOrigin','=',$cityOrigin)
                ->where('nationality','=',$nationality)
                ->first();

                if ($confirmUpdate != null)
                {
                    return response()->json([
                        'success' => true,
                        'user' => $confirmUpdate,
                        'error' => ''
                    ]);
                }
                else
                {
                    return response()->json([
                        'success' => false,
                        'user' => '',
                        'error' => 'DB'
                    ]);
                }
            }
            else
            {
                return response()->json([
                    'success' => false,
                    'user' => '',
                    'error' => 'user'
                ]);
            }  
        }
        else
        {
            return response()->json([
                'success' => false,
                'user' => '',
                'error' => 'data'
            ]);
        }  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
