<?php

namespace App\Http\Controllers;

use App\Models\Program;
use App\Models\ProgramByUser;
use App\Models\User;
use Illuminate\Http\Request;

class ProgramByUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programs = ProgramByUser::getStundetnsByProgram();

        return response()->json([
            'programs' => $programs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_user = $request->id_user ? $request->id_user : null;
        $id_program = $request->id_program ? $request->id_program : null;
        
        if ($id_user && $id_program)
        {
            $user = User::find($id_user);
            $program = Program::find($id_program);
    
            if ($user != null && $program != null)
            {
                $existsPBU = ProgramByUser::where('id_user','=',$id_user)
                ->where('id_program','=',$id_program)
                ->get();
    
                if ($existsPBU->isEmpty())
                {
                    $ProgramByUser = new ProgramByUser();
                    $ProgramByUser->id_user = $id_user;
                    $ProgramByUser->id_program = $id_program;
                    $ProgramByUser->save();
    
                    $confirmPBU = ProgramByUser::where('id_user','=',$id_user)
                    ->where('id_program','=',$id_program)
                    ->get();
    
                    if (!$confirmPBU->isEmpty())
                    {
                        return response()->json([
                            'success' => true,
                            'error' => ''
                        ]);
                    }
                    else
                    {
                        return response()->json([
                            'success' => false,
                            'error' => 'DB'
                        ]);
                    }
                }
                else
                {
                    return response()->json([
                        'success' => false,
                        'error' => 'ProgramByUser'
                    ]);
                }
            }
            else
            {
                return response()->json([
                    'success' => false,
                    'error' => 'register'
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'error' => 'data'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
