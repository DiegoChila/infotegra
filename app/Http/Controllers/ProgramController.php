<?php

namespace App\Http\Controllers;

use App\Models\Program;
use App\Models\ProgramByUser;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programs = Program::get()->all();

        return response()->json([
            'programs' => $programs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $program = $request->program ? $request->program : null;
        $semesters = $request->semesters ? $request->semesters : null;
        $campus = $request->campus ? $request->campus : null;
        $faculty = $request->faculty ? $request->faculty : null;
        $image = $request->file('image') ? $request->file('image') : null;

        if ($program && $semesters && $campus && $faculty && $image)
        {
            if (is_numeric($semesters))
            {
                $imgSrc = '';
                $newProgram = new Program();
                $newProgram->program = $program;
                $newProgram->semesters = $semesters;
                $newProgram->campus = $campus;
                $newProgram->faculty = $faculty;
                $newProgram->imgSrc = $imgSrc;
                $newProgram->save();

                $confirmProgram = Program::where('program','=',$program)
                ->where('semesters','=',$semesters)
                ->where('campus','=',$campus)
                ->where('faculty','=',$faculty)
                ->where('imgSrc','=',$imgSrc)
                ->first();

                if ($confirmProgram!=null)
                {
                    $date = Carbon::now();
                    $date = $date->subHours(5);
                    $date = $date->format('Y-m-d_H-i-s');        
                    $image->move('uploads/img/programs/'.$confirmProgram->id.'/'.$date.'/', $image->getClientOriginalName());
                    $url = 'http://127.0.0.1:8000/'.'uploads/img/programs/'.$confirmProgram->id.'/'.$date.'/'.$image->getClientOriginalName();
                    $confirmProgram->imgSrc = $url;
                    $confirmProgram->save();

                    return response()->json([
                        'success' => true,
                        'program' => $confirmProgram,
                        'error' => ''
                    ]);
                }
                else
                {
                    return response()->json([
                        'success' => false,
                        'program' => '',
                        'error' => 'DB'
                    ]);
                }
            }
            else
            {
                return response()->json([
                    'success' => false,
                    'program' => '',
                    'error' => 'semester'
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'program' => '',
                'error' => 'data'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $program = Program::find($id);

        if ($program != null)
        {
            return response()->json([
                'success' => true,
                'program' => $program
            ]);
        }
        else
        {
            return response()->json([
                'success' => false,
                'program' => ''
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $program = Program::find($id);

        if ($program!=null)
        {
            $program->program = $request->program ? $request->program : $program->program;
            $program->semesters = $request->semesters ? (is_numeric($request->semesters) ? $request->semesters : $program->semesters) : $program->semesters;
            $program->campus = $request->campus ? $request->campus : $program->campus;
            $program->faculty = $request->faculty ? $request->faculty : $program->program;

            $program->save();

            return response()->json([
                'success' => true,
                'program' => $program,
                'error' => ''
            ]);
        }
        else
        {
            return response()->json([
                'success' => false,
                'program' => '',
                'error' => 'id'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $programsUser = ProgramByUser::where('id_program','=',$id)->get();

        foreach ($programsUser as $p)
        {
            $p->delete();
        }

        $program = Program::find($id);
        $program->delete();

        $confirmDelete = Program::find($id);

        if ($confirmDelete==null)
        {
            return response()->json([
                'success' => true,
                'id' => $id
            ]);
        }
        else
        {
            return response()->json([
                'success' => false,
                'id' => ''
            ]);
        }
    }

    public function uploadImage(Request $request)
    {
        $image = $request->file('image') ? $request->file('image') : null;
        $id = $request->id ? $request->id : null;

        if ($image && $id)
        {
            $date = Carbon::now();
            $date = $date->subHours(5);
            $date = $date->format('Y-m-d_H-i-s');        
            $image->move('uploads/img/programs/'.$id.'/'.$date.'/', $image->getClientOriginalName());
            $url = 'http://127.0.0.1:8000/'.'uploads/img/programs/'.$id.'/'.$date.'/'.$image->getClientOriginalName();

            $program = Program::find($id);
            $program->imgSrc = $url;
            $program->save();
            
            $programSave = Program::where('id','=',$id)
            ->where('imgSrc','=',$url)
            ->first();
    
            if ($programSave!=null)
            {
                return response()->json([
                    'success' => true,
                    'imgSrc' => $programSave->imgSrc,
                    'error' => '',
                ]); 
            }
            else
            {
                return response()->json([
                    'success' => false,
                    'imgSrc' => '',
                    'error' => 'DB'
                ]); 
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'imgSrc' => '',
                'error' => 'data'
            ]);
        }
    }
}
