<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProgramByUserController;
use App\Http\Controllers\ProgramController;
use App\Http\Controllers\UserController;
use App\Models\ProgramByUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/validateCaptcha', [AuthController::class, 'validateCaptcha']);



Route::resource('program', ProgramController::class, ['only' => [
    'index'
]]);

Route::group(['middleware' => 'auth.jwt',], function () {
    Route::post('/program/uploadImage', [ProgramController::class, 'uploadImage']);
    Route::resource('program', ProgramController::class, ['except' => [
        'index'
    ]]);
    Route::resource('user', UserController::class);
    Route::resource('programbyuser', ProgramByUserController::class);
});
