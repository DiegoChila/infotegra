create database infotegra;

use infotegra;

create table `role`(
	`id` int(11) auto_increment not null,
    `role` varchar(50) not null,
    primary key(`id`)
);

create table `users`(
	`id` int(11) auto_increment not null,
    `name` varchar(150) not null,
    `lastName` varchar(150) not null,
    `code` varchar(50) not null,
    `direction` varchar(500) not null,
    `tel` int(50) not null,
    `cityResidence` varchar(150) not null,
    `cityOrigin` varchar(150) not null,
    `nationality` varchar(150) not null,
    `role` int(11) not null,
    `email` varchar(150) not null,
    `password` varchar(250) not null,
    primary key(`id`),
    foreign key(`role`) references `role`(`id`)
);

create table `program`(
	`id` int(11) auto_increment not null,
    `program` varchar(400) not null,
    `semesters` int(2) not null,
    `campus` varchar(200) not null,
    `faculty` varchar(200) not null,
    `imgSrc` varchar(250) not null,
    primary key(`id`)
);

create table `programByUser`(
	`id` int(11) auto_increment not null,
    `id_user` int(11) not null,
    `id_program` int(11) not null,
    primary key(`id`),
    foreign key(`id_user`) references `users`(`id`),
    foreign key(`id_program`) references `program`(`id`)
);

insert into `role`(`role`) values ('admin'), ('student');